import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2' 

import numpy as np
import xarray as xr
import ecubevis as ecv
import dl4ds as dds
import scipy as sp
import netCDF4 as nc
import climetlab as cml

import tensorflow as tf 
from tensorflow import keras
from tensorflow.keras import models

#########
cmlds_train = cml.load_dataset("maelstrom-downscaling", dataset="training")
cmlds_val = cml.load_dataset("maelstrom-downscaling", dataset="validation")
cmlds_test = cml.load_dataset("maelstrom-downscaling", dataset="testing")
#########
t2m_hr_train = cmlds_train.to_xarray().t2m_tar
t2m_hr_test = cmlds_test.to_xarray().t2m_tar
t2m_hr_val = cmlds_val.to_xarray().t2m_tar

t2m_lr_train = cmlds_train.to_xarray().t2m_in
t2m_lr_test = cmlds_test.to_xarray().t2m_in
t2m_lr_val = cmlds_val.to_xarray().t2m_in

z_hr_train = cmlds_train.to_xarray().z_tar
z_hr_test = cmlds_test.to_xarray().z_tar
z_hr_val = cmlds_val.to_xarray().z_tar

z_lr_train = cmlds_train.to_xarray().z_in
z_lr_test = cmlds_test.to_xarray().z_in
z_lr_val = cmlds_val.to_xarray().z_in

########
t2m_scaler_train = dds.StandardScaler(axis=None)
t2m_scaler_train.fit(t2m_hr_train)  
y_train = t2m_scaler_train.transform(t2m_hr_train)
y_test = t2m_scaler_train.transform(t2m_hr_test)
y_val = t2m_scaler_train.transform(t2m_hr_val)

x_train = t2m_scaler_train.transform(t2m_lr_train)
x_test = t2m_scaler_train.transform(t2m_lr_test)
x_val = t2m_scaler_train.transform(t2m_lr_val)

z_scaler_train = dds.StandardScaler(axis=None)
z_scaler_train.fit(z_hr_train)  
y_z_train = z_scaler_train.transform(z_hr_train)
y_z_test = z_scaler_train.transform(z_hr_test)
y_z_val = z_scaler_train.transform(z_hr_val)

x_z_train = z_scaler_train.transform(z_lr_train)
x_z_test = z_scaler_train.transform(z_lr_test)
x_z_val = z_scaler_train.transform(z_lr_val)

##########3

y_train = y_train.expand_dims(dim='channel', axis=-1)
y_test = y_test.expand_dims(dim='channel', axis=-1)
y_val = y_val.expand_dims(dim='channel', axis=-1)

x_train = x_train.expand_dims(dim='channel', axis=-1)
x_test = x_test.expand_dims(dim='channel', axis=-1)
x_val = x_val.expand_dims(dim='channel', axis=-1)

y_z_train = y_z_train.expand_dims(dim='channel', axis=-1)
y_z_test = y_z_test.expand_dims(dim='channel', axis=-1)
y_z_val = y_z_val.expand_dims(dim='channel', axis=-1)

x_z_train = x_z_train.expand_dims(dim='channel', axis=-1)
x_z_test = x_z_test.expand_dims(dim='channel', axis=-1)
x_z_val = x_z_val.expand_dims(dim='channel', axis=-1)

########


ARCH_PARAMS = dict(n_filters=8,
                   n_blocks=8,
                   normalization=None,
                   dropout_rate=0.0,
                   dropout_variant='spatial',
                   attention=False,
                   activation='relu',
                   localcon_layer=True)

trainer = dds.SupervisedTrainer(
    backbone='resnet',
    upsampling='spc', 
    data_train=y_train, 
    data_val=y_val,
    data_test=y_test,
    data_train_lr=None, # here you can pass the LR dataset for training with explicit paired samples
    data_val_lr=None, # here you can pass the LR dataset for training with explicit paired samples
    data_test_lr=None, # here you can pass the LR dataset for training with explicit paired samples
    scale=8,
    time_window=None, 
    static_vars=None,
    predictors_train=[y_z_train],
    predictors_val=[y_z_val],
    predictors_test=[y_z_test],
    interpolation='inter_area',
    patch_size=None, 
    batch_size=60, 
    loss='mae',
    epochs=100, 
    steps_per_epoch=None, 
    validation_steps=None, 
    test_steps=None, 
    learning_rate=(1e-3, 1e-4), lr_decay_after=1e4,
    early_stopping=False, patience=6, min_delta=0, 
    save=False, 
    save_path=None,
    show_plot=True, verbose=True, 
    device='GPU', 
    **ARCH_PARAMS)

trainer.run()

### ####################################################
### Inference

pred = dds.Predictor(
    trainer, 
    y_test, 
    scale=8, 
    array_in_hr=True,
    static_vars=None, 
    predictors=[y_z_test], 
    time_window=None,
    interpolation='inter_area', 
    batch_size=8,
    scaler=t2m_scaler_train,
    save_path=None,
    save_fname=None,
    return_lr=True,
    device='CPU')

unscaled_y_pred, coarsened_array = pred.run()

# ######################################
# Save

print (type(unscaled_y_pred), type (coarsened_array))
np.save('unscaled_y_pred.npy', unscaled_y_pred)
np.save('coarsened_array.npy', coarsened_array)

unscaled_y_test = t2m_scaler_train.inverse_transform(y_test)
np.save('unscaled_y_test.npy', unscaled_y_test)

print ("Finished")
